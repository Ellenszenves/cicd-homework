out_list = []
with open('output.txt', 'r') as out_text:
    for i in out_text:
        out_list.append(i)
with open('index.html', 'w') as index_site:
    index_site.write('<html>')
    index_site.write('\n')
    index_site.write('<head>')
    index_site.write('\n')
    index_site.write('<title>Homework pipeline site</title>')
    index_site.write('\n')
    index_site.write('</head>')
    index_site.write('\n')
    index_site.write('<body>')
    index_site.write('\n')
    index_site.write('<h1>Output numbers:</h1>')
    index_site.write('\n')
    for item in out_list:
        index_site.write('<p>')
        index_site.write(item)
        index_site.write('</p>')
        index_site.write('\n')
    index_site.write('</body>')
    index_site.write('\n')
    index_site.write('</html>')
in_list = []
out_list = []
with open('input.txt', 'r') as input_text:
    for item in input_text:
        in_list.append(item)
with open('output.txt', 'r') as output_text:
    for item in output_text:
        out_list.append(item)
counter = 0
for i in in_list:
    assert int(i) * 2 == int(out_list[counter])
    #print(out_list[counter])
    counter = counter + 1
my_list = []
with open('input.txt', 'r') as input_file:
    for row in input_file:
        result = int(row) * 2
        my_list.append(result)
with open('output.txt', 'w') as output_file:
    for row in my_list:
        output_file.write(str(row))
        output_file.write('\n')